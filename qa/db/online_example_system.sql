/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50723
Source Host           : localhost:3306
Source Database       : online_example_system

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2019-02-18 09:47:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for answer
-- ----------------------------
DROP TABLE IF EXISTS `answer`;
CREATE TABLE `answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_by` int(11) NOT NULL,
  `content` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `question_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `answer_ibfk_1` (`question_id`),
  CONSTRAINT `answer_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of answer
-- ----------------------------
INSERT INTO `answer` VALUES ('44', '7', '旦撒', '2019-01-31 16:16:02', '40');

-- ----------------------------
-- Table structure for aqroom
-- ----------------------------
DROP TABLE IF EXISTS `aqroom`;
CREATE TABLE `aqroom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '答疑室名称',
  `create_by` int(11) NOT NULL,
  `create_date` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `aqroom_ibfk_1` (`create_by`),
  CONSTRAINT `aqroom_ibfk_1` FOREIGN KEY (`create_by`) REFERENCES `user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aqroom
-- ----------------------------
INSERT INTO `aqroom` VALUES ('4', '大声的', '6', '2019-01-31 16:14:31');

-- ----------------------------
-- Table structure for question
-- ----------------------------
DROP TABLE IF EXISTS `question`;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `create_by` int(11) NOT NULL,
  `describe` varchar(255) NOT NULL COMMENT '问题描述',
  `create_date` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `aqroom_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `question_ibfk_1` (`aqroom_id`),
  KEY `create_by` (`create_by`),
  CONSTRAINT `question_ibfk_1` FOREIGN KEY (`aqroom_id`) REFERENCES `aqroom` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `question_ibfk_2` FOREIGN KEY (`create_by`) REFERENCES `user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of question
-- ----------------------------
INSERT INTO `question` VALUES ('40', '的撒旦 ', '6', '啊D阿斯顿', '2019-01-31 16:15:54', '4');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `authority` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', '管理员', 'admin');
INSERT INTO `role` VALUES ('2', '教师', 'teacher');
INSERT INTO `role` VALUES ('3', '学生', 'student');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(255) NOT NULL COMMENT '用户角色',
  PRIMARY KEY (`id`),
  KEY `username` (`username`),
  KEY `role` (`role_id`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('6', '管理员', 'admin', '$2a$04$F9Stw/mS6RnMgunS36.uBO2YCxkJM0TIc8r1ZieRACQ3R.XgcEr/a', '1');
INSERT INTO `user` VALUES ('7', '邰志敏', 'taizhimin', '$2a$04$Wy2rL.NOiGliMOJ6JR8rhubORFIfBia4hJz2J3HXpuQEoYBNyr4yu', '2');
INSERT INTO `user` VALUES ('17', '学生', 'student', '$2a$04$CWdP60caxl1JGrvvvuSDN.skzKcqVK6i8RPYjOe9CMHdrMr8rfcFy', '3');
