package com.xunzhishimin.qa.dao;

import com.xunzhishimin.qa.entity.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleDao extends CrudRepository<Role, Long> {
}
