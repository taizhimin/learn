package com.xunzhishimin.qa.dao;

import com.xunzhishimin.qa.entity.Aqroom;
import org.springframework.data.repository.CrudRepository;

public interface AqroomDao extends CrudRepository<Aqroom, Long> {
}
