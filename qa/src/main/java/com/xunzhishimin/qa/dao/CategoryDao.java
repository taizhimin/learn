/**
 * Copyright &copy; 2017-2022 <a href="http://rxida.com">Rxida</a> All rights reserved.
 */
package com.xunzhishimin.qa.dao;

import com.xunzhishimin.qa.entity.Aqroom;
import com.xunzhishimin.qa.entity.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * 栏目DAO接口
 */
public interface CategoryDao extends CrudRepository<Category, Long> {

    List<Category> findCategoryByModule(String module);

    List<Category> findCategoryByParentAndInMenu(String parentId, String inMenu);

}
