package com.xunzhishimin.qa.dao;

import com.xunzhishimin.qa.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserDao extends CrudRepository<User, Long> {
    User findUserByUsername(String usernmae);
}
