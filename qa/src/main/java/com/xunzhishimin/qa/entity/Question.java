package com.xunzhishimin.qa.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * 问题
 */
@Entity
@Table(name = "question")
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;//问题编号
    @Column(name = "[title]")
    private String title;//问题标题
    @OneToOne
    private Aqroom aqroom;//答疑室0479-8267363
    @OneToOne
    @JoinColumn(name = "create_by")
    private User createBy;//问题创建者
    @Column(name = "[describe]")
    private String describe;//问题描述
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createDate;//问题创建时间
    @OneToMany
    @JoinColumn(name = "question_id", referencedColumnName = "id")
    private List<Answer> answers;//回答数

    public Question() {
        super();
    }

    public Question(long id) {
        super();
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getCreateBy() {
        return createBy;
    }

    public void setCreateBy(User createBy) {
        this.createBy = createBy;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Aqroom getAqroom() {
        return aqroom;
    }

    public void setAqroom(Aqroom aqroom) {
        this.aqroom = aqroom;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }
}
