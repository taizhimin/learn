package com.xunzhishimin.qa.web;

import com.xunzhishimin.qa.dao.AnswerDao;
import com.xunzhishimin.qa.dao.AqroomDao;
import com.xunzhishimin.qa.dao.QuestionDao;
import com.xunzhishimin.qa.entity.Answer;
import com.xunzhishimin.qa.entity.Aqroom;
import com.xunzhishimin.qa.entity.Question;
import com.xunzhishimin.qa.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

@Controller
public class QuestionController {
    @Autowired
    private QuestionDao questionDao;
    @Autowired
    private AqroomDao aqRoomDao;
    @Autowired
    private AnswerDao answerDao;

    @RequestMapping("/aqroom/{id}")
    public String index(@PathVariable("id") long id, Model model) {
        model.addAttribute("aqroom", aqRoomDao.findById(id).get());
        model.addAttribute("list", questionDao.findQuestionByAqroom_Id(id));
        return "aq";
    }

    @RequestMapping("/question/{id}")
    public String question(@PathVariable("id") long id, Model model) {
        model.addAttribute("question", questionDao.findById(id).get());
        model.addAttribute("list", answerDao.findAnswerByQuestion_Id(id));
        return "q";
    }

    @GetMapping("/question/form/{id}")
    @PreAuthorize(value = "hasAnyAuthority('admin')")
    public String questionForm(@PathVariable("id") long id, Model model) {
        model.addAttribute("question", questionDao.findById(id).get());
        return "questionForm";
    }

    @GetMapping("/question/add/{id}")
    public String questionAdd(@PathVariable("id") long id, Model model) {
        Question question = new Question();
        question.setAqroom(new Aqroom(id));
        model.addAttribute("question", question);
        return "questionForm";
    }

    @PostMapping("/question/save")
    @PreAuthorize(value = "hasAnyAuthority('admin')")
    public String question(Question question, Model model) {
        if (question.getId() == null) {
            User user = (User) SecurityContextHolder.getContext()
                    .getAuthentication()
                    .getPrincipal();
            question.setCreateDate(new Date());
            question.setCreateBy(user);
        } else {
            Question question1 = questionDao.findById(question.getId()).get();
            question.setCreateBy(question1.getCreateBy());
            question.setAqroom(question1.getAqroom());
            question.setCreateDate(question1.getCreateDate());
            question.setAnswers(question1.getAnswers());
        }
        questionDao.save(question);
        return "redirect:/aqroom/" + question.getAqroom().getId();
    }

    @GetMapping("/question/delete/{id}")
    @PreAuthorize(value = "hasAnyAuthority('admin')")
    public String delete(@PathVariable("id") long id, Model model) {
        String url = "redirect:/aqroom/" + questionDao.findById(id).get().getAqroom().getId();
        questionDao.delete(new Question(id));
        return url;
    }

    @PostMapping("/answer")
    public String answer(Answer answer, Model model) {
        if (answer.getId() == null) {
            User user = (User) SecurityContextHolder.getContext()
                    .getAuthentication()
                    .getPrincipal();
            answer.setCreateBy(user);
            answer.setCreateDate(new Date());
        } else {
            Answer answer1 = answerDao.findById(answer.getId()).get();
            answer.setCreateDate(answer1.getCreateDate());
            answer.setCreateBy(answer1.getCreateBy());
            answer.setQuestion(answer1.getQuestion());
        }
        answerDao.save(answer);
        return "redirect:/question/" + answer.getQuestion().getId();
    }

    @GetMapping("/answer/delete/{id}")
    @PreAuthorize(value = "hasAnyAuthority('admin')")
    public String answerDelete(@PathVariable("id") long id, Model model) {
        String url = "redirect:/question/" + answerDao.findById(id).get().getQuestion().getId();
        answerDao.delete(new Answer(id));
        return url;
    }
}
