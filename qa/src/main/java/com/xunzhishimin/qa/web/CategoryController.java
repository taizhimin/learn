package com.xunzhishimin.qa.web;

import com.xunzhishimin.qa.dao.CategoryDao;
import com.xunzhishimin.qa.entity.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller("/category")
public class CategoryController {
    @Autowired
    private CategoryDao categoryDao;

    @GetMapping({"/list", ""})
    public String list(Model model) {
        model.addAttribute("list", categoryDao.findAll());
        return "categoryList";
    }

    @GetMapping("/modify/{id}")
    public String modify(@PathVariable("id") long id, Model model) {
        model.addAttribute("category", categoryDao.findById(id));
        return "categoryForm";
    }

    @GetMapping("/add")
    public String add() {
        return "categoryForm";
    }

    @GetMapping("/save")
    public String save(Category category) {
        categoryDao.save(category);
        return "redirect:/category";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id) {
        categoryDao.deleteById(id);
        return "redirect:/category";
    }
}
