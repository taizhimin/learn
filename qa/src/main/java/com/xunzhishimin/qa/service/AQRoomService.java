package com.xunzhishimin.qa.service;

import com.xunzhishimin.qa.dao.AqroomDao;
import com.xunzhishimin.qa.entity.Aqroom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AQRoomService {
    @Autowired
    private AqroomDao dao;

    public Iterable<Aqroom> findList() {
        return dao.findAll();
    }
}
