package com.example.oauth.dao;

import com.example.oauth.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserDao extends CrudRepository<User, Long> {
    User findUserByUsername(String usernmae);
}
