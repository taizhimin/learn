package com.example.oauth.common;

/**
 * json返回数据
 */
public class JsonResult {
    public static String ERROR = "1";
    public static String SUCCESS = "0";
    private String code;//返回代码
    private String msg;//返回消息
    private Object data;//返回数据

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
